/* Copyright 2023 ddiniz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "keymap_brazilian_abnt2.h"


// Defines names for use in layer keycodes and the keymap
enum layer_names {
    BASE,
    FUNCTION_KEYS
};

enum custom_keycodes {
    M_PLAY_PAUSE = SAFE_RANGE,
    M_STOP,
    M_PREV,
    M_NEXT,
    M_RAND,
    M_RAND_MODE,
    M_NORMAL_MODE,
    M_VOL_UP,
    M_VOL_DOWN,
    
    STR0,
    STR1,
    STR2,
    STR3,

    CARET_ADD_MODE,
    CARET_SELECTION
};

#define M_PP M_PLAY_PAUSE
#define M_RMOD M_RAND_MODE
#define M_NMOD M_NORMAL_MODE
#define M_VUP M_VOL_UP
#define M_VDW M_VOL_DOWN
#define C_ADD CARET_ADD_MODE
#define C_ALL CARET_SELECTION

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /* Base 
    * ,------------------------------------------------.                                                         ,------------------------------------------------.
    * |  ESC |   '  |   1  |   2  |   3  |   4  |   5  |                                                         |   6  |   7  |   8  |   9  |   0  |   -  |   =  |
    * |------+------+------+------+------+------+------|                                                         |------+------+------+------+------+------+------|
    * | M_PP | TAB  |   Q  |   W  |   E  |   R  |   T  |                                                         |   Y  |   U  |   I  |   O  |   P  |   `  |   [  |
    * |------+------+------+------+------+------+------|                                                         |------+------+------+------+------+------+------|
    * |M_PREV| CAPS |   A  |   S  |   D  |   F  |   G  |                                                         |   H  |   J  |   K  |   L  |   Ç  |   ~  |   ]  |
    * |------+------+------+------+------+------+------|             ,-------------. ,-------------.             |------+------+------+------+------+------+------|
    * |M_NEXT| SHIFT|   Z  |   X  |   C  |   V  |   B  |             | WIN  |      | |      | WIN  |             |   N  |   M  |   ,  |   .  |   ;  |   /  |  \   |
    * |------+------+------+------+------+------+------'      ,------+------+------| |------+------+------.      '------+------+------+------+------+------+------|
    * |M_RAND|M_VUP |M_RMOD| STR0 | STR1 |  MOD |             |PRTSCR| LALT | S+A  | |      | LALT |   *  |             |  MOD | HOME |  UP  |  END | PGUP | RALT |
    * |------+------+------+------+------+------'      ,------+------+------+------| |------+------+------+------.      '------+------+------+------+------|------|
    * |M_STOP|M_DWN |M_NMOD| DEL  |ENTER |             | SPACE|BCKSPC| LCTRL| C_ALL| |      | RCTRL| DEL  |ENTER |             | LEFT | DOWN | RIGHT| PGDW | RCTRL|
    * '----------------------------------'             '---------------------------' '---------------------------'             '----------------------------------'
    */
    [0] = LAYOUT(
        KC_ESC, BR_QUOT, KC_1,   KC_2,   KC_3,     KC_4,  KC_5,                            KC_NO,         KC_NO,                            KC_6, KC_7,   KC_8,    KC_9,     KC_0,     KC_MINUS,     KC_EQL,
        M_PP,   KC_TAB,  KC_Q,   KC_W,   KC_E,     KC_R,  KC_T,                            LSFT(KC_LALT), KC_NO,                            KC_Y, KC_U,   KC_I,    KC_O,     KC_P,     BR_ACUT,      BR_LBRC,
        M_PREV, KC_CAPS, KC_A,   KC_S,   KC_D,     KC_F,  KC_G,                            C_ALL,         KC_NO,                            KC_H, KC_J,   KC_K,    KC_L,     BR_CCED,  BR_TILD,      BR_RBRC,
        M_NEXT, KC_LSFT, KC_Z,   KC_X,   KC_C,     KC_V,  KC_B,                   KC_LGUI,                       KC_NUM,                    KC_N, KC_M,   KC_COMM, KC_DOT,   BR_SCLN,  BR_SLSH,      BR_BSLS,
        M_RAND, M_VUP,   M_RMOD, STR0,   STR1,     OSL(1),               KC_PSCR, KC_LALT,                       KC_LALT, KC_PAST,                OSL(1), KC_HOME, KC_UP,    KC_END,   KC_PAGE_UP,   KC_RALT,
        M_STOP, M_VDW,   M_NMOD, KC_DEL, KC_ENTER,              KC_BSPC, KC_SPC,  KC_LCTL,                       KC_RCTL, KC_ENTER, KC_DEL,               KC_LEFT, KC_DOWN,  KC_RIGHT, KC_PAGE_DOWN, KC_RCTL
    ),
    /* Function keys 
    * ,------------------------------------------------.                                                         ,------------------------------------------------.
    * |  ESC |      |  F1  |  F2  |  F3  |  F4  |  F5  |                                                         |  F6  |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |
    * |------+------+------+------+------+------+------|                                                         |------+------+------+------+------+------+------|
    * |      |      |      |      |      |      |      |                                                         |      |      |      |      |      |      |      |
    * |------+------+------+------+------+------+------|                                                         |------+------+------+------+------+------+------|
    * |      |      |      |      |      |      |      |                                                         |      |      |      |      |      |      |      |
    * |------+------+------+------+------+------+------|             ,-------------. ,-------------.             |------+------+------+------+------+------+------|
    * |      |      |      |      |      |      |      |             | WIN  |      | |      | NUM  |             |      |      |      |      |      |      |      |
    * |------+------+------+------+------+------+------'      ,------+------+------| |------+------+------.      '------+------+------+------+------+------+------|
    * |      |      |      |      |      |  MOD |             |PRTSCR| LALT |      | |      | RALT |  *   |             |  MOD |      |      |      |      |      |
    * |------+------+------+------+------+------'      ,------+------+------+------| |------+------+------+------.      '------+------+------+------+------+------|
    * |      |      |      |      |      |             | SPACE|BCKSPC| LCTRL|      | |      | RCTRL| DEL  |ENTER |             |      |      |      |      |      |
    * '----------------------------------'             '---------------------------' '---------------------------'             '----------------------------------'
    */
    [1] = LAYOUT(
        TG(1), KC_NO, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5,                            KC_NO, KC_NO,                            KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12,
        KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,                            KC_NO, KC_NO,                            KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,  KC_NO,  KC_NO,
        KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,                            KC_NO, KC_NO,                            KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,  KC_NO,  KC_NO,
        KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,                   KC_LGUI,               KC_NUM,                    KC_NO, KC_NO, KC_NO, KC_NO,  KC_NO,  KC_NO,  KC_NO,
        KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, TG(1),                 KC_PSCR, KC_LALT,               KC_LALT, KC_PAST,                 TG(1), KC_NO, KC_NO,  KC_NO,  KC_NO,  KC_NO,
        KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,               KC_BSPC, KC_SPC,  KC_LCTL,               KC_RCTL, KC_ENTER, KC_DEL,               KC_NO, KC_NO,  KC_NO,  KC_NO,  KC_NO
    ),
};

//xdotool key --window $( xdotool search --limit 1 --all --pid $( pgrep balabolka ) --name foobar2000 ) "ctrl+shift+1"

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
    case M_PLAY_PAUSE :
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"1"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_STOP:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"4"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_PREV:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"3"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_NEXT:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"2"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_RAND:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"5"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_RAND_MODE:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"6"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_VOL_UP:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"7"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_VOL_DOWN:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"8"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case M_NORMAL_MODE:
        if(record->event.pressed){
            SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"9"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;

    case STR0:
        if(record->event.pressed){
            //SEND_STRING("for i"SS_TAP(X_SLASH)"=0"SS_LSFT(X_SLASH)"i<"SS_LSFT(X_SLASH)"i++" SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT));
        }else{
            //released
        }
        break;

    case STR1:
        if(record->event.pressed){
            //SEND_STRING("for i,val"SS_TAP(X_SLASH)"=range ");
        }else{
            //released
        }
        break;

    case STR2:
        if(record->event.pressed){
            //SEND_STRING("if err!=nil"SS_LSFT(X_RBRC)"}");
        }else{
            //released
        }
        break;

    case STR3:
        if(record->event.pressed){
            //SEND_STRING("fmt.Sprintf");
        }else{
            //released
        }
        break;

    case CARET_SELECTION:
        if(record->event.pressed){
           SEND_STRING(SS_DOWN(X_LCTL)SS_DOWN(X_LSFT)"L"SS_UP(X_LCTL)SS_UP(X_LSFT));
        }else{
            //released
        }
        break;
    }
    return true;
};

