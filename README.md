# qmk_firmware_split96
This is an implementation of split96, a handwired split keyboard with 96 keys, that uses two arduino pro micros.
Download and compile qmk_firmware's source code, follow CLI installation instructions (not GUI), drop the split96 folder on keyboards/handwired/split96 and run the following commands:
	sudo apt-get install gcc unzip wget zip gcc-avr binutils-avr avr-libc dfu-programmer dfu-util gcc-arm-none-eabi binutils-arm-none-eabi libnewlib-arm-none-eabi avrdude
	sudo python3 -m pip install qmk
BUILD:
	qmk compile -kb handwired/split96 -km default
FLASH:
	qmk flash -kb handwired/split96 -km default

To send hotkeys to wine applications, use the send_hotkey.sh script included in this folder, like this

./send_hotkey.sh "{application name}" "{shortcut}"

On your linux system keyboard shorcuts