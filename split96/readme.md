# split96

An 96-key split with mdf cut case and powered with Pro Micro controllers.

* Full build documented here: [https://github.com/ddiniz/split96](https://gitlab.com/ddiniz/qmk_firmware_split96)  

* Keyboard Maintainer: [ddiniz](https://gitlab.com/ddiniz)
* Hardware Supported: ATmega32U4 Pro Micro
